﻿
using UnityEngine;

public abstract class State
{
    protected StateMachine stateMachine;

    public abstract void StateUpdate();
    public virtual void OnStateEnter() { }
    public virtual void OnStateExit() { }

    public State(StateMachine stateMachine)
    {
        this.stateMachine = stateMachine;
    }

    protected void debuglog(string message)
    {
        LogManager.Debug(stateMachine.GetType(), stateMachine.gameObject, message);
    }
}
