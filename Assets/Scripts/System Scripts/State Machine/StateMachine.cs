﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    protected State currentState;

    protected virtual void Update()
    {
        if (currentState == null) Debug.LogError(name + ": State Machine does not transition into a start during Start()");
        currentState.StateUpdate();
    }

    public void StateTransition(State state)
    {
        if (currentState != null)
            currentState.OnStateExit();

        currentState = state;

        if (currentState != null)
            currentState.OnStateEnter();
    }

    protected void debuglog(string message)
    {
        LogManager.Debug(this.GetType(), gameObject, message);
    }
}
