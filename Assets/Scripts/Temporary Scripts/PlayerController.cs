﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
#pragma warning disable 649 // disable warning for private field assigned but not used.
    
    //Private fields not accessible in the editor
    private SceneManager sceneManager;
    private Camera playerCamera;
    private ActorController playerActor;
    private GameObject mouseOverTarget;
    private Vector3Int moveDestination;
    private Vector3 mouseWorldPos;

    void Start()
    {
        getComponents();
    }

    void Update()
    {
        mouseWorldPos = playerCamera.ScreenToWorldPoint(Input.mousePosition);

        playerCamera.transform.position = new Vector3(playerActor.transform.position.x, playerActor.transform.position.y, playerCamera.transform.position.z);

        switch (sceneManager.SceneState)
        {
            case SceneManager.FSMSceneState.Exploration:
                {
                    switch (playerActor.ActorState)
                    {
                        case ActorController.fsmActorState.MayAct:
                            {
                                //Mouseinput needs to be updated to address above plus be flexible enough to address all state-based conditions identified below

                                ///Decisions to make about how mouseover / tilepath etc work:
                                ///1. Must: Whether to draw tilemap or not when mousing over an entity target; depends on whether you are shooting or need close proximity (use object / melee attack).
                                ///1a. Must: Also hide bracket for destination tile when doing this, it's clear which tile you will move to from the last dot in the path.
                                ///2. Must: What mouse cursor icon to show while mousing over an object. Cursor for nothing, hand for interact, sword for melee, and crosshair for shoot?
                                ///3. Should: Pop-up UI text to show the name of the thing you're mousing over would be good. Could pop-up near the cursor or at bottom of screen
                                ///4. Could: Floating contextual menu if you right click on an entity - activate, hack, attack, inspect, open, etc. could be presented based on the interactions
                                ///available on that entity
                                mouseInput();

                                if (Input.GetMouseButtonDown(0))
                                {
                                    if (mouseOverTarget)
                                    {
                                        if (mouseOverTarget.GetComponent<InteractComponent>())
                                        {
                                            if (playerActor.IsInActivateRange(mouseOverTarget.GetComponent<InteractComponent>()))
                                            {
                                                playerActor.ActionActivate(mouseOverTarget.GetComponent<InteractComponent>());
                                            }
                                            else
                                            {
                                                debuglog(name + ": Player attmpted to activate " + mouseOverTarget.name + " but too far away");
                                            }
                                        }
                                    }
                                    else
                                        setMoveTargetExploration();
                                }
                                break;
                            }
                        case ActorController.fsmActorState.Moving:
                            {
                                TilemapManager.MyInstance.UpdateTileMapPath(playerActor);

                                //In exploration mode, while moving (which can be multiple tiles with a single click), keep drawing the tile path to the point where you clicked
                                //Do not update the tilepath location to match mouse location until the move is finished or interrupted
                                //If mouse click while moving, stop moving after you finish arriving in the next cell, then switch to mayact state again
                                //Option to consider - hiding mouse cursor while moving. Maybe check what stoneshard does.
                                break;
                            }
                        case ActorController.fsmActorState.Meleeing:
                            {
                                //Can you even melee in exploration mode? I guess you could bash objects..?
                                //hide tilemap path while shooting
                                //keep cursor as melee icon until finished
                                //Player cannot input to influence their actor while in this state, but maybe can still open ingame menus etc
                                break;
                            }
                        case ActorController.fsmActorState.Shooting:
                            {
                                //I think you should be able to shoot in exploration mode - shooting doors etc. It could make noise and attract enemies thus starting combat
                                //hide tilemap path while shooting
                                //keep cursor as shooting icon until finished
                                //Player cannot input to influence their actor while in this state, but maybe can still open ingame menus etc
                                break;
                            }
                        case ActorController.fsmActorState.Waiting:
                            {
                                //I can't think of why the player actor would be in this state during exploration mode. No need for any input code here.
                                mouseInput();
                                break;
                            }
                        case ActorController.fsmActorState.Dying:
                            {
                                //Hide tilemap path while dying
                                TilemapManager.MyInstance.ClearTileMapPath(playerActor);
                                //Do not run mouseover code - just have default mouse icon
                                break;
                            }
                        case ActorController.fsmActorState.Dead:
                            {
                                //Hide tilemap path when dead
                                TilemapManager.MyInstance.ClearTileMapPath(playerActor);
                                //Do not run mouseover code - just have default mouse icon
                                break;
                            }
                    }
                    break;
                }
            case SceneManager.FSMSceneState.Combat:
                {
                    switch (playerActor.ActorState)
                    {
                        case ActorController.fsmActorState.MayAct:
                            {
                                //Run all normal mouseover / tilemap path code in this state
                                mouseInput();

                                if (Input.GetKeyDown(KeyCode.Space))
                                {
                                    playerActor.ActionSkipTurn();
                                    break;
                                }

                                if (Input.GetMouseButtonDown(0))
                                {
                                    if (mouseOverTarget)
                                    {
                                        //This will need to change to account for multiple action types depending on what kind of entity
                                        //Also will need to move player if the interactions requires adjacency and you aren't adjacent
                                        //Need to check if the actor has visibility or line of sight of the target and don't shoot if not
                                        //This should be based on UIstate which is determined by what's being moused over, and will determine what icon + action is done when left click
                                        if (mouseOverTarget.GetComponent<InteractComponent>())
                                        {
                                            if (playerActor.IsInActivateRange(mouseOverTarget.GetComponent<InteractComponent>()))
                                            {
                                                playerActor.ActionActivate(mouseOverTarget.GetComponent<InteractComponent>());
                                            }
                                            else
                                            {
                                                debuglog(name + ": Player attmpted to activate " + mouseOverTarget.name + " but too far away");
                                            }
                                        }
                                        else
                                        {
                                            //TODO add check here to see if hostile. If so, only allow to shoot if ctrl is held down
                                            if (mouseOverTarget != gameObject) //do not shoot yourself
                                            {
                                                if (playerActor.IsInProjectileRange(mouseOverTarget.GetComponent<AttackableComponent>()) && mouseOverTarget.GetComponent<AttackableComponent>().enabled)
                                                {
                                                    playerActor.ActionShoot(mouseOverTarget.GetComponent<AttackableComponent>());
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if(AStarManager.MyInstance.GetMyPath(playerActor) != null)
                                            setMoveTargetCombat();
                                    }
                                    break;
                                }

                                if (Input.GetMouseButtonDown(1))
                                {
                                    if (mouseOverTarget)
                                    {
                                        if (Vector3.Distance(playerActor.transform.position, mouseOverTarget.transform.position) <= 1.5f && mouseOverTarget.GetComponent<AttackableComponent>().enabled)
                                        {
                                            debuglog(name + ": Player using melee attack on " + mouseOverTarget.name);
                                            playerActor.ActionMelee(mouseOverTarget.GetComponent<AttackableComponent>());
                                        }
                                    }
                                }
                                break;
                            }
                        case ActorController.fsmActorState.Moving:
                            {
                                //In combat mode you can only move a single tile, so it's fine to hide the tilepath entirely while moving, it's quick and will prevent the player from trying
                                //to click while still moving and unable to move
                                //Likewise I think it's fine to hide the mouse cursor completely in this state. Moving a single tile is quick
                                break;
                            }
                        case ActorController.fsmActorState.Meleeing:
                            {
                                //hide tilemap path while shooting
                                //keep cursor as melee icon until finished
                                //Player cannot input to influence their actor while in this state, but maybe can still open ingame menus etc
                                break;
                            }
                        case ActorController.fsmActorState.Shooting:
                            {
                                //hide tilemap path while shooting
                                //keep cursor as shooting icon until finished
                                //Player cannot input to influence their actor while in this state, but maybe can still open ingame menus etc
                                break;
                            }
                        case ActorController.fsmActorState.Waiting:
                            {
                                //It looks like stoneshard suppresses all player input etc while the enemies act, but the enemies all act simultaneously
                                //If we are going to have a per-actor turn order, we might want to retain the ability for the player to run any input while waiting as long as it
                                //doesn't cause their actor to act
                                mouseInput();
                                break;
                            }
                        case ActorController.fsmActorState.Dying:
                            {
                                //Hide tilemap path while dying
                                TilemapManager.MyInstance.ClearTileMapPath(playerActor);
                                //Do not run mouseover code - just have default mouse icon
                                break;
                            }
                        case ActorController.fsmActorState.Dead:
                            {
                                //Hide tilemap path when dead
                                TilemapManager.MyInstance.ClearTileMapPath(playerActor);
                                //Do not run mouseover code - just have default mouse icon
                                break;
                            }
                    }
                    break;
                }
        }
    }

    private void setMoveTargetCombat()
    {
        if (AStarManager.MyInstance.GetMyPath(playerActor) != null)
        {
            moveDestination = AStarManager.MyInstance.GetMyPath(playerActor).Peek();
            playerActor.ActionMove(moveDestination);
        }
        else
            debuglog(name + ": setMoveTarget() called but Astar returned a null value. This could be because you clicked without a valid path or it could be something is broken.");
    }

    private void setMoveTargetExploration()
    {
        //The code from moving in exploration is kinda split across player and actor. review this once you have AI moving in exploration
        if (AStarManager.MyInstance.GetMyPath(playerActor) != null)
        {
            moveDestination = AStarManager.MyInstance.GetMyPath(playerActor).Peek(); 
            playerActor.ActionMove(moveDestination);
        }
        else
            debuglog(name + ": setMoveTarget() called but Astar returned a null value. This could be because you clicked without a valid path or it could be something is broken.");
    }

    //Update this to work with a new mouseover.cs script component that can be put on either actors or environmental stuff, whether clickable or not
    //Would be great if mouseover.cs could optionally pop up the name of the thing you're hovering over - a bool option that triggers a UI event
    private bool mouseOverCheck()
    {
        if (mouseOverTarget)
        {
            mouseOverTarget.GetComponent<MouseOverComponent>().RemoveHighlight();
            mouseOverTarget = null;
        }

        //Fix layermask reference to capture this error during startup
        RaycastHit2D hit = Physics2D.Raycast(playerCamera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, LayerMask.GetMask("Entities"));

        if (hit.collider != null)
        {
            if (hit.collider.GetComponent<MouseOverComponent>())
            {
                if(hit.collider.GetComponent<MouseOverComponent>().enabled && hit.collider.GetComponent<MouseOverComponent>().Targetable)
                {
                    if (hit.collider.GetComponent<ActorController>())
                    {
                        if (playerActor.CheckLineofSight(hit.collider.GetComponent<VisibleComponent>()))
                        {
                            mouseOverTarget = hit.collider.gameObject;
                            mouseOverTarget.GetComponent<MouseOverComponent>().Highlight();
                        }
                    }
                    else //if it's an itemcontroller or other activatable object
                    {
                        mouseOverTarget = hit.collider.gameObject;
                        mouseOverTarget.GetComponent<MouseOverComponent>().Highlight();
                    }
                }
            }
            else
                Debug.LogError("Mouseover occured on " + hit.collider.name + " in MouseOver layer but the object does not have an MouseOver component");
        }

        if (mouseOverTarget) return true;
        else return false;
    }

    void mouseInput()
    {
        //Only run code for the tilemap update based on mouse position if you are not hovering mouse over an entity
        if (!mouseOverCheck())
        {
            //Fix layermask reference to capture this error during startup
            RaycastHit2D hit = Physics2D.Raycast(playerCamera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, LayerMask.GetMask("TileMap"));
            if (hit.collider != null)
            {
                TilemapManager.MyInstance.DrawTileMapPath(playerActor, mouseWorldPos);
            }
            else
                TilemapManager.MyInstance.ClearTileMapPath(playerActor);
        }
        else
            TilemapManager.MyInstance.ClearTileMapPath(playerActor);
    }

    void getComponents()
    {
        if (FindObjectsOfType<PlayerController>().Length != 1)
            Debug.LogError("Found more than one playercontroller.cs instance. There can only be one object with this script.");

        if (GetComponent<ActorController>())
        {
            playerActor = GetComponent<ActorController>();
        }
        else
            Debug.LogError(name + ": Didn't find an actorcontroller.cs script on its gameobject.");

        if (FindObjectsOfType<SceneManager>().Length == 1)
        {
            sceneManager = FindObjectOfType<SceneManager>();
        }
        else
            Debug.LogError(name + " found too few or too many scenemanagers");

        if (FindObjectsOfType<Camera>().Length == 1)
        {
            playerCamera = FindObjectOfType<Camera>();
        }
        else
            Debug.LogError(name + " found too few or too many cameras");
    }

    protected void debuglog(string message)
    {
        LogManager.Debug(this.GetType(), gameObject, message);
    }
}
