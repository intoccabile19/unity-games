﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapManager : MonoBehaviour
{
#pragma warning disable 649 // disable warning for private field assigned but not used.
    
    [SerializeField]
    private Tilemap aStarTilemap;

    [SerializeField]
    private AnimatedTile pathTile;

    [SerializeField]
    private Tile[] tiles;

    private HashSet<Vector3Int> changedTiles = new HashSet<Vector3Int>();

    private Camera playerCamera;

    private static TilemapManager instance;

    public static TilemapManager MyInstance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<TilemapManager>();
            }

            return instance;
        }
    }

    void Start()
    {
        errorChecks();
        fields();
        createGameObjects();
    }

    void Update()
    {
        
    }

    private void drawPath(Stack<Vector3Int> path)
    {
        if (path != null)
        {
            resetTiles();
            Stack<Vector3Int> copyOfPath = new Stack<Vector3Int>(new Stack<Vector3Int>(path));
            while (copyOfPath.Count > 0)
            {
                Vector3Int position = copyOfPath.Pop();

                if (copyOfPath.Count == 0)
                {
                    aStarTilemap.SetTile(position, tiles[1]);
                    aStarTilemap.SetColor(position, new Color(1f, 1f, 1f, 1f));
                    changedTiles.Add(position);
                }
                else
                {
                    aStarTilemap.SetTile(position, pathTile);
                    aStarTilemap.SetColor(position, new Color(1f, 1f, 1f, 1f));
                    changedTiles.Add(position);
                }
            }
        }
        else
            Debug.LogError(name + ": Something called DrawPath but the path provided was null");
    }

    public void DrawTileMapPath(ActorController actor, Vector3 mouseWorldPos)
    {
        Stack<Vector3Int> path = AStarManager.MyInstance.RequestNewPath(actor, mouseWorldPos);
        if (path != null)
            drawPath(path);
        else debuglog(name + ": Requested a path from Astar.cs but returned null.");
        
    }

    public void UpdateTileMapPath(ActorController actor)
    {
        drawPath(AStarManager.MyInstance.GetMyPath(actor));
    }

    //Move this to TilemapManager but still call it from this script
    public void ClearTileMapPath(ActorController playerActor)
    {
        AStarManager.MyInstance.ResetMyPath(playerActor);
        resetTiles();
    }

    private void resetTiles()
    {
        foreach (Vector3Int position in changedTiles)
        {
            aStarTilemap.SetTile(position, tiles[3]);
            aStarTilemap.SetColor(position, new Color(1f, 1f, 1f, 0f));
        }

        changedTiles.Clear();
    }

    private void errorChecks()
    {
        bool errorFound = false;

        if (FindObjectsOfType<Camera>().Length != 1)
        {
            Debug.LogError(name + " found too few or too many cameras");
            errorFound = true;
        }
        if (errorFound)
        {
            Debug.LogError(name + ": errorChecks() found at least one error that will cause an exception. Quitting.");
            UnityEditor.EditorApplication.isPlaying = false;
        }
    }

    private void fields()
    {
        playerCamera = FindObjectOfType<Camera>();        
    }

    private void createGameObjects()
    {
        Tilemap catcher = Instantiate(aStarTilemap);
        catcher.transform.parent = gameObject.transform;
        catcher.name = "AstarRaycastCatcherTilemap";
        catcher.gameObject.layer = 8;
    }

    protected void debuglog(string message)
    {
        LogManager.Debug(this.GetType(), gameObject, message);
    }
}
