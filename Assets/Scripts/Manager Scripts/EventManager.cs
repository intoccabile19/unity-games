﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Events;

public class Event_TurnManager : UnityEvent<TurnManager>
{
}

public class EventManager : MonoBehaviour
{
    public static Event_TurnManager TurnManagerInstantiated;

    private bool tmTest = false;

    void Awake()
    {
        if (TurnManagerInstantiated == null)
            TurnManagerInstantiated = new Event_TurnManager();
    }

    void Update()
    {
        if (!tmTest)
        {
            GameObject tm = new GameObject();
            tm.name = "TurnManager";
            tm.AddComponent<TurnManager>();
            tmTest = true;
        }
    }

    private void testyoureventfires()
    {
        Debug.Log("event fired");
    }
}
