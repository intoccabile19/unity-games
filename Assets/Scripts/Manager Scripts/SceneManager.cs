﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
#pragma warning disable 649 // disable warning for private field assigned but not used.

    //TODO 22 May: Remove scenestate once turnmanager.cs is implemented.
    //TODO 22 May: Scenemanager to instantiate other managers

    public enum FSMSceneState
    {
        Exploration, Combat
    }

    public FSMSceneState SceneState = FSMSceneState.Combat;

    private List<GameObject> turnOrder = new List<GameObject>();

    private int currentTurn = 0;

    void Start()
    {
    }

    void Update()
    {
        switch (SceneState)
        {

            case FSMSceneState.Combat:
                {
                    if (turnOrder.Count == 1)
                    {
                        debuglog(name + ": Entering Exploration state as there are no enemies left.");
                        SceneState = FSMSceneState.Exploration;
                    }
                        
                    break;
                }
            case FSMSceneState.Exploration:
                {
                    break;
                }
        }
    }

    //TODO 22 May: Remove this method once turnmanager.cs is implemented.
    public void AddToTurnOrder(GameObject actor)
    {
        //This method allows any player or enemy object to automatically register themselves upon Start() to have a turn in the turn order.
        //This avoids us needing to manually ensure enemies are added to the scenemanager in the Unity editor.
        //below is temp code to make the player go first
        if (!turnOrder.Contains(actor))
        {
            if (actor.GetComponent<PlayerController>())
                turnOrder.Insert(0, actor);
            else
                turnOrder.Add(actor);
        }
        else
            Debug.LogError(name + ": " + actor.name + " is trying to add itself to turn order more than once.");
    }

    //TODO 22 May: Remove this method once turnmanager.cs is implemented.
    public bool IsItMyTurn(GameObject requestor)
    {
        bool result = false;

        switch (SceneState)
        {
            case FSMSceneState.Combat:
                {
                    if (turnOrder[currentTurn] == requestor)
                        result = true;
                    else result = false;
                        break;
                }
            case FSMSceneState.Exploration:
                {
                    result = false;
                    Debug.LogWarning(requestor.name + " is checking to see whether it can have a turn in combat, but the SceneManager is in exploration mode.");
                    break;
                }
        }
        return result;
    }

    //TODO 22 May: Remove this method once turnmanager.cs is implemented.
    public void FinishMyTurn(GameObject requestor)
    {
        switch (SceneState)
        {
            case FSMSceneState.Combat:
                {
                    if (turnOrder[currentTurn] != requestor)
                    {
                        debuglog(name + ": " + requestor.name + " is trying to end its turn when it's not its turn");
                    }
                    else
                    {
                        currentTurn++;
                        if (currentTurn > turnOrder.Count - 1) currentTurn = 0;
                        debuglog(name + ": It is now " + turnOrder[currentTurn].name + "'s turn");
                    }
                    break;
                }
            case FSMSceneState.Exploration:
                {
                    Debug.LogWarning(requestor.name + " is trying to ends its turn, but the SceneManager is in exploration mode.");
                    break;
                }
        }
    }

    //TODO 22 May: Remove this method once turnmanager.cs is implemented.
    public void RemoveFromTurnOrder(GameObject requestor)
    {
        switch (SceneState)
        {
            case FSMSceneState.Combat:
                {
                    if (!turnOrder[currentTurn] == requestor)
                        turnOrder.Remove(requestor);
                    else
                    {
                        currentTurn++;
                        if (currentTurn > turnOrder.Count - 2) currentTurn = 0;
                        turnOrder.Remove(requestor);
                    }
                    break;
                }
            case FSMSceneState.Exploration:
                {
                    Debug.LogWarning(requestor.name + " is telling the scenemanager that its dead, but the SceneManager is in exploration mode.");
                    break;
                }
        }        
    }

    protected void debuglog(string message)
    {
        LogManager.Debug(this.GetType(), gameObject, message);
    }
}
