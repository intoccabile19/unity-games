﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Event_Int : UnityEvent<int>
{
}

public class AudioManager : MonoBehaviour
{
#pragma warning disable 649 // disable warning for private field assigned but not used.
    
    static public Event_Int PlayClip;

    [SerializeField]
    private AudioClip defaultClip;

    [SerializeField]
    private AudioClip gunshot;

    [SerializeField]
    private AudioClip meleeAttack;

    [SerializeField]
    private AudioClip movement;

    [SerializeField]
    private AudioClip death;

    [SerializeField]
    private AudioClip activate;

    private AudioSource audioSource;

    void Start()
    {
        if (!GetComponent<AudioSource>())
            Debug.LogError("Gameobject " + name + ": Script AudioManager.cs: No audiosource component attached.");
        else audioSource = GetComponent<AudioSource>();

        if (PlayClip == null)
            PlayClip = new Event_Int();

        PlayClip.AddListener(playSound);
    }

    private void playSound(int clipID)
    {
        AudioClip clip = defaultClip;

        //In the long run, you'd want your ID numbers for the sounds to be the filenames, so you can do away with this code as you add more sounds and just find and play the sound file in the asset library
        //with the name passed in from the event.
        //You'd use a category system with 1##s to be movement, 2##s to be shooting, etc so you can scale it easily without wasting any time
        //something like that anyway.
        switch (clipID)
        {
            case 1:
                {
                    clip = movement;
                    break;
                }
            case 2:
                {
                    clip = gunshot; 
                    break;
                }
            case 3:
                {
                    clip = meleeAttack;
                    break;
                }
            case 4:
                {
                    clip = death;
                    break;
                }
            case 5:
                {
                    clip = activate;
                    break;
                }
        }
        audioSource.PlayOneShot(clip);
    }
}
