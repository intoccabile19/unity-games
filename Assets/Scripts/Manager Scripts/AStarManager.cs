﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class AStarManager : MonoBehaviour
{
#pragma warning disable 649 // disable warning for private field assigned but not used.
    
    [SerializeField]
    private Tilemap aStarMap;

    [SerializeField]
    private Tilemap unwalkableTileMap;

    private Vector3Int currentCell, destinationCell;

    private Node current;

    private HashSet<Node> openList;
    private HashSet<Node> closedList;

    private List<Vector3Int> unwalkableTiles = new List<Vector3Int>();

    private Dictionary<ActorController, Stack<Vector3Int>> paths = new Dictionary<ActorController, Stack<Vector3Int>>();

    private Dictionary<Vector3Int, Node> allNodes = new Dictionary<Vector3Int, Node>();

    private static AStarManager instance;

    public static AStarManager MyInstance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<AStarManager>();
            }

            return instance;
        }
    }

    void Start()
    {
        foreach (Vector3Int position in unwalkableTileMap.cellBounds.allPositionsWithin)
        {
            if (!unwalkableTileMap.HasTile(position))
            {
                unwalkableTiles.Add(position);
            }
        }
    }

    void Update()
    {
    }

    public Stack<Vector3Int> GetMyPath(ActorController actor)
    {
        Stack<Vector3Int> value = new Stack<Vector3Int>();
        paths.TryGetValue(actor, out value);
        return value;
    }

    public Stack<Vector3Int> RequestNewPath(ActorController actor, Vector3 destinationPosition)
    {
        if (paths.Keys.Contains(actor))
        {
            paths.Remove(actor);
        }

        destinationCell = aStarMap.WorldToCell(destinationPosition);

        //TODO 18 May: if detination cell is occupied, return a cell next to it instead?

        resetFields();

        Vector3 currentPosition = actor.transform.position;

        currentCell = new Vector3Int((int)currentPosition.x, (int)currentPosition.y, (int)currentPosition.z);

        Stack<Vector3Int> path = Algorithm();

        paths.Add(actor, path);

        return path;
    }

    public void ResetMyPath(ActorController actor)
    {
        if (paths.Keys.Contains(actor))
        {
            paths.Remove(actor);
        }
    }

    private void Initialise()
    {

        current = GetNode(currentCell);

        openList = new HashSet<Node>();

        closedList = new HashSet<Node>();

        openList.Add(current);
    }

    private Stack<Vector3Int> Algorithm()
    {
        if (current == null)
        {
            Initialise();
        }

        Stack<Vector3Int> workingPath = null;

        while (openList.Count > 0 && workingPath == null)
        {
            List<Node> neighbors = FindNeighbors(current.Position);

            ExamineNeighbors(neighbors, current);

            UdpateCurrentTile(ref current);

            workingPath = GeneratePath(current);
        }
        return workingPath;
    }

    private List<Node> FindNeighbors(Vector3Int parentPosition)
    {
        List<Node> neighbors = new List<Node>();
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                Vector3Int neighborPos = new Vector3Int(parentPosition.x - x, parentPosition.y - y, parentPosition.z);
                if (y != 0 || x != 0)
                {
                    if (neighborPos != currentCell && unwalkableTiles.Contains(neighborPos) && aStarMap.GetTile(neighborPos))
                    {
                        if (!isCellOccupied(neighborPos))
                        {
                            Node neighbor = GetNode(neighborPos);
                            neighbors.Add(neighbor);
                        }
                    }
                }
            }
        }

        return neighbors;
    }

    //TODO: This should be called in a much more efficient spot, it's re-running on the same tiles hundreds of times in a single Astar path gen instance
    private bool isCellOccupied(Vector3Int cellPosition)
    {
        //TODO 18 May: Remove this bool check and false return once there is a way to return the 'cell next door'
        if (cellPosition == destinationCell)
            return false;

        Vector3 raycastPos = new Vector3(cellPosition.x + 0.5f, cellPosition.y + 0.5f, cellPosition.z);
        
        RaycastHit2D hit = Physics2D.Raycast(raycastPos, Vector2.zero, Mathf.Infinity, LayerMask.GetMask("Entities"));

        if (hit.collider != null)
        {
            if(hit.collider.GetComponent<OccupiesGridComponent>())
                if(hit.collider.GetComponent<OccupiesGridComponent>().enabled)
                    return true;
        }
        return false;
    }

    private void ExamineNeighbors(List<Node> neighbors, Node current)
    {
        for (int i = 0; i < neighbors.Count; i++)
        {

            Node neighbor = neighbors[i];

            int gScore = DetermineGScore(neighbors[i].Position, current.Position);

            if (openList.Contains(neighbor))
            {
                if (current.G + gScore < neighbor.G)
                {
                    CalcValues(current, neighbor, gScore);
                }
            }
            else if (!closedList.Contains(neighbor))
            {
                CalcValues(current, neighbor, gScore);

                openList.Add(neighbor);
            }
        }
    }

    private void CalcValues(Node parent, Node neighbor, int cost)
    {
        neighbor.Parent = parent;

        neighbor.G = parent.G + cost;

        neighbor.H = ((Math.Abs((neighbor.Position.x - destinationCell.x)) + Math.Abs((neighbor.Position.y + destinationCell.y))) * 10);

        neighbor.F = neighbor.G + neighbor.H;
    }

    private int DetermineGScore(Vector3Int neighbor, Vector3Int current)
    {
        int gScore = 0;

        int x = current.x - neighbor.x;
        int y = current.y - neighbor.y;

        if (Math.Abs(x - y) % 2 == 1)
        {
            gScore = 10;
        }
        else
        {
            gScore = 14;
        }

        return gScore;
    }

    private void UdpateCurrentTile(ref Node current)
    {
        openList.Remove(current);
        closedList.Add(current);

        if (openList.Count > 0)
        {
            current = openList.OrderBy(x => x.F).First();
        }
    }

    private Node GetNode(Vector3Int position)
    {
        if (allNodes.ContainsKey(position))
        {
            return allNodes[position];
        }
        else
        {
            Node node = new Node(position);
            allNodes.Add(position, node);
            return node;
        }
    }

    private Stack<Vector3Int> GeneratePath(Node current)
    {
        if (current.Position == destinationCell)
        {
            Stack<Vector3Int> finalPath = new Stack<Vector3Int>();

            while (current.Position != currentCell)
            {
                finalPath.Push(current.Position);

                current = current.Parent;
            }

            return finalPath;
        }

        return null;
    }

    private void resetFields()
    {
        if (openList != null) openList.Clear();
        if (closedList != null) closedList.Clear();
        if (allNodes != null) allNodes.Clear();
        
        current = null;
    }
}

