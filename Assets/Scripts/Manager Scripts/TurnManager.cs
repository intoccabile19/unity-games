﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour
{

    private List<ActorController> turnOrder = new List<ActorController>();

    private int currentTurn = 0;


    void Start()
    {
        InitialiseCombat();
    }

    void Update()
    {
        
    }

    void InitialiseCombat()
    {
        //This method will kick off combat when a combat event occurs.

        //1. Invite all actors to register themselves
        EventManager.TurnManagerInstantiated.Invoke(this);

        //2. Determine turn order
        if (turnOrder.Count > 0)
        {

            //Could improve this to create some kind of initiative system
        }
        else
        {
            //If there are no combatants, get rid of the turnmanager.
            Destroy(gameObject);
        }
    }

    public void AddToTurnOrder(ActorController actor)
    {
        if (!turnOrder.Contains(actor))
        {
            if (actor.GetComponent<PlayerController>())
                turnOrder.Insert(0, actor);
            else
                turnOrder.Add(actor);
        }
        else
            Debug.LogError(name + ": " + actor.name + " is trying to add itself to turn order more than once.");
    }

    public void RemoveFromTurnOrder(ActorController actor)
    {
        if (!turnOrder[currentTurn] == actor)
            turnOrder.Remove(actor);
        else
        {
            currentTurn++;
            if (currentTurn > turnOrder.Count - 2) currentTurn = 0;
                turnOrder.Remove(actor);
        }
    }

    public bool IsItMyTurn(ActorController actor)
    {
        bool result = false;
                    if (turnOrder[currentTurn] == actor)
                        result = true;
                    else result = false;
        return result;
    }

    public void FinishMyTurn(ActorController actor)
    {
        if (turnOrder[currentTurn] != actor)
        {
            debuglog(name + ": " + actor.name + " is trying to end its turn when it's not its turn");
        }
        else
        {
            currentTurn++;
            if (currentTurn > turnOrder.Count - 1) currentTurn = 0;
            debuglog(name + ": It is now " + turnOrder[currentTurn].name + "'s turn");
        }
    }

    protected void debuglog(string message)
    {
        LogManager.Debug(this.GetType(), gameObject, message);
    }
}
