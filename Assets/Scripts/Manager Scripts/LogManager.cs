﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LogManager : MonoBehaviour
{
#pragma warning disable 649 // disable warning for private field assigned but not used.
    
    private enum DebugMode { Disabled, RequireOne, RequireBoth }

    [SerializeField]
    private DebugMode debugMode;

    private static DebugMode mode;

    private static List<Type> types = new List<Type>();

    private void Awake()
    {
        mode = debugMode;

        foreach (var component in GetComponents<Component>())
        {
            if (component != this)
            {
                types.Add(component.GetType());
            }
        }

        foreach (var component in GetComponents<MonoBehaviour>())
        {
            if (component != this)
                component.enabled = false;
        }
    }

    static public void Debug(Type type, GameObject gameObject, string message)
    {
        if(whetherToLog(type, gameObject))
            UnityEngine.Debug.Log(message);
    }

    static public void Combat(string message)
    {
        //TODO 20 May - implement combat log logic. Define strings in scriptableobjects, not in code.
    }

    private static bool whetherToLog(Type type, GameObject gameObject)
    {
        switch (mode)
        {
            case DebugMode.Disabled:
                {
                    return false;
                }
            case DebugMode.RequireBoth:
                {
                    if (types.Contains(type) && gameObject.GetComponent<DebugObject>())
                        return true;
                    break;
                }
            case DebugMode.RequireOne:
                {
                    if (types.Contains(type) | gameObject.GetComponent<DebugObject>())
                        return true;
                    break;
                }
        }
        return false;
    }
}
