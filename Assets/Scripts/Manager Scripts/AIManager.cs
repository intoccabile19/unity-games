﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;

public class AIManager : MonoBehaviour
{
#pragma warning disable 649 // disable warning for private field assigned but not used.
    
    //Private fields not accessible in the editor
    private AttackableComponent currentTargetAttackableComponent;
    private ActorController currentTargetActor;
    private List<ActorController> actorList = new List<ActorController>();

    void Start()
    {      
        getComponents();
    }

    void Update()
    {
        if (currentTargetAttackableComponent == null && currentTargetActor != null)
            currentTargetAttackableComponent = currentTargetActor.GetComponent<AttackableComponent>();
        if (currentTargetActor == null)
            Debug.LogError(name + ": No target actor for AI");
        //TODO: 17 May: Remove logic here checking into actor state. The actor itself should disable its attackablecomponent once it's in these states
        //if (currentTargetActor != null && currentTargetActor.ActorState != ActorController.fsmActorState.Dying && currentTargetActor.ActorState != ActorController.fsmActorState.Dead)
        foreach (ActorController actor in actorList)
        {
            if (actor.ActorState == ActorController.fsmActorState.MayAct)
            {
                if (currentTargetAttackableComponent.enabled)
                {
                    if (actor.CanMelee && actor.CheckLineofSight(currentTargetAttackableComponent.GetComponent<VisibleComponent>()) && actor.IsInMeleeRange(currentTargetAttackableComponent))
                    {
                        debuglog(name + ": Telling " + actor.name + " to melee at " + currentTargetAttackableComponent.name);
                        actor.ActionMelee(currentTargetAttackableComponent);
                    }
                    else if (actor.CanShoot && actor.CheckLineofSight(currentTargetAttackableComponent.GetComponent<VisibleComponent>()) && actor.IsInProjectileRange(currentTargetAttackableComponent))
                    {
                        debuglog(name + ": Telling " + actor.name + " to shoot at " + currentTargetAttackableComponent.name);
                        actor.ActionShoot(currentTargetAttackableComponent);
                    }
                    else
                    {
                        //Would be great if we could make the AI move to a spot that it knows it will have LOS, but that's a bit more of a complex use of sight checks and astar. Do that much, much later.
                        debuglog(name + ": " + actor.name + " isn't close enough or doesn't have line of sight to shoot or to melee at " + currentTargetAttackableComponent.name + ". Moving instead");

                        AStarManager.MyInstance.RequestNewPath(actor, currentTargetActor.transform.position);

                        if (AStarManager.MyInstance.GetMyPath(actor) != null)
                        {
                            Vector3Int moveDestination = getMoveTarget(actor);
                            actor.ActionMove(moveDestination);
                        }
                        else
                        {
                            debuglog(name + ": " + actor.name + " no path. Skipping turn");
                            actor.ActionSkipTurn();
                        }
                    }
                }
                else
                {
                    debuglog(name + ": telling + " + actor.name + " to skip turn as the player's attackablecomponent is disabled");
                    actor.ActionSkipTurn();
                }
            }
        }
    }

    private Vector3Int getMoveTarget(ActorController actor)
    {
        Vector3Int moveDestination = new Vector3Int(0,0,0);
        int i = 1;
        foreach (Vector3Int position in AStarManager.MyInstance.GetMyPath(actor))
        {
            if (i != 1)
            {
                i++;
            }
            else
            {
                moveDestination = position;
                i++;
            }
        }
        return moveDestination;
    }

    public void RegisterActor(ActorController actor)
    {
        actorList.Add(actor);
    }

    public void UnregisterActor(ActorController actor)
    {
        actorList.Remove(actor);
    }

    void getComponents()
    {
        if (FindObjectsOfType<PlayerController>().Length != 1) 
            Debug.LogError(name + ": Too few or too many playercontrollers, can't choose one to focus on");
        else
            currentTargetActor = FindObjectOfType<PlayerController>().GetComponent<ActorController>();
    }

    protected void debuglog(string message)
    {
        LogManager.Debug(this.GetType(), gameObject, message);
    }
}
