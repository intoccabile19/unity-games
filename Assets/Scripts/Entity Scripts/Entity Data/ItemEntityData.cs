﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class ItemEntityData : ScriptableObject
{
    //This scriptableobject should include all data required to customise/build an item that can be placed on the tilemap for the player to collect

    [Header("UI")]
    public bool MouseOverable = true; //If set to true, this entity will change color when the mouse hovers over it and actions like attacking / activating will be possible

    [Header("Tilemap")]
    public bool OccupiesGrid = true; //If set to true, this entity will prevent movement in the grid tile it occupies

    [Header("Sight")]
    public bool Visible = true; //If set to true, this entity will be drawn on screen and is a valid target for line of sight checks
    public bool BlocksLineOfSight = false; //If set to true, this entity will block line of sight beyond itself. For example this would be disabled for most actors, but enabled for a door.

    [Header("Interaction")]
    public bool Interactable = true; //If set to true, actors will be able to interact with the entity, which could do anything from collecting an item to opening a door etc

    //Below here are fields for single-use pickups. The intention is to have items collected deposit into the player inventory but until that is possible, simple pickup is better than nothing.
    public enum EnumPickupType
    {
        Health, Ammo, Unlock
    }

    [Header("Temp Pickup Fields - until inventory is developed")]
    public EnumPickupType PickupType;
    public int PickupValue;
}
