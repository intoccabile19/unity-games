﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class ActorEntityData : ScriptableObject
{
    //This scriptableobject should include all data required to customise/build an actor

    [Header("UI")]
    public bool MouseOverable = true; //If set to true, this entity will change color when the mouse hovers over it and actions like attacking / activating will be possible
    
    [Header("Tilemap")]
    public bool OccupiesGrid = true; //If set to true, this entity will prevent movement in the grid tile it occupies

    [Header("Sight")]
    public bool Visible = true; //If set to true, this entity will be drawn on screen and is a valid target for line of sight checks
    public bool BlocksLineOfSight = false; //If set to true, this entity will block line of sight beyond itself. For example this would be disabled for most actors, but enabled for a door.

    [Header("Defensive/Healing")]
    public int MaxHealth; //Probably should redefine these as a health pool object and move the logic inside it
    public int CurrentHealth; //Probably should redefine these as a health pool object and move the logic inside it
    public bool Attackable;
    public bool Healable;
    public bool Repairable;
    public int Defense;

    [Header("Attacking")]
    public int Aim;
    [Header("Attacking - Ranged")] 
    public bool CanShoot;
    public ProjectileController Prefab_Projectile;
    public float ProjectileRange;
    [Header("Attacking - Melee")] 
    public bool CanMelee;
    //create meleecontroller class
    public GameObject Prefab_Melee;

    //Add faction here

    //Maybe stuff here to do with AudibleComponent
}

