﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Event_Actor : UnityEvent<ActorController>
{
}

public class InteractComponent : MonoBehaviour
{
#pragma warning disable 649 // disable warning for private field assigned but not used.

    public Event_Actor Interaction;

    void Start()
    {
        if (Interaction == null)
            Interaction = new Event_Actor();
        //activatable should be mutually exclusive with actor
        if (GetComponent<ActorController>())
            Debug.LogError(name + ": Gameobject has both " + this + " and ActorController component");
    }

    void Update()
    {
        
    }

    public void Interact(ActorController usedByActor)
    {
        //Unsatisfactory that I can't check for the number of listeners here
        Interaction.Invoke(usedByActor);
    }
}
