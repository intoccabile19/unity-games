﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibleComponent : MonoBehaviour
{
    public bool BlocksLineOfSight;

    void Start()
    {
        //Keep this to allow for this component to have disable/enable checkbox in editor
    }
}
