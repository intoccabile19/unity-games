﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairableComponent : MonoBehaviour
{
    //Still deciding whether this class should get a reference to the entity data and perform operations / return values via get accessors for the underlying data, or
    //leave this to the actor/item/whatever entity class, and this class just fires events

    public Event_Int Event_Repaired = new Event_Int();

    void Start()
    {
        //Keep this to allow for this component to have disable/enable checkbox in editor
    }

    public void Heal(int amount)
    {
        Event_Repaired.Invoke(amount);
    }
}
