﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseOverComponent : MonoBehaviour
{
    public bool Targetable = true;

    private SpriteRenderer spriteRenderer;
    private Color normalColor;

    void Start()
    {
        errorChecks();
        fields();
    }

    void Update()
    {
        
    }

    public void Highlight()
    {
        if (Targetable)
        {
            spriteRenderer.color = Color.cyan;
        }
        else Debug.LogError(name + ": Highlight() being called even though this MouseOverComponent has 'Targetable' set false.");
    }

    public void RemoveHighlight()
    {
        float alpha = spriteRenderer.color.a;
        Color newcolor = normalColor;
        newcolor.a = alpha;
        spriteRenderer.color = newcolor;
    }

    private void errorChecks()
    {
        bool errorFound = false;

        if (GetComponentsInChildren<SpriteRenderer>().Length != 1)
        {
            Debug.LogError(name + ": Found too few or too many spriterenderers on my child objects");
            errorFound = true;
        }
        if (!GetComponent<Collider2D>())
        {
            Debug.LogError(name + ": No collider component attached to gameobject.");
            errorFound = true;
        }
        if (errorFound)
        {
            Debug.LogError(name + ": errorChecks() found at least one error that will cause an exception or break stuff. Quitting.");
            UnityEditor.EditorApplication.isPlaying = false;
        }
    }

    void fields()
    {
        spriteRenderer = GetComponentsInChildren<SpriteRenderer>()[0];
        normalColor = spriteRenderer.color;
    }
}
