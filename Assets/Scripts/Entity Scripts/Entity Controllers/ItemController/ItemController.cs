﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : StateMachine
{
    //Private fields to make accessible in the editor
    [SerializeField]
    private ItemEntityData itemEntityDataSource;

    //Private fields not accessible in the editor
    private ItemEntityData itemEntityDataInstance;

    void Start()
    {
        getEntityInstanceData();

        errorChecks();

        fields();

        StateTransition(new ItemControllerState_Ready(this));
    }

    protected override void Update()
    {
        base.Update();
    }

    //TODO 19 MAY: Consider moving this method content into the state rather than checking for the state here.
    private void pickUpItem(ActorController usedByActor)
    {
        //Temp code to have different effects until inventory is implemented
        if (currentState is ItemControllerState_Ready)
        {
            switch (itemEntityDataInstance.PickupType)
            {
                case ItemEntityData.EnumPickupType.Ammo:
                    {
                        //Replenish ammo
                        break;
                    }
                case ItemEntityData.EnumPickupType.Health:
                    {
                        usedByActor.GetComponent<HealableComponent>().Heal(itemEntityDataInstance.PickupValue);
                        debuglog(name + ": Healed player for " + itemEntityDataInstance.PickupValue);
                        break;
                    }
                case ItemEntityData.EnumPickupType.Unlock:
                    {
                        //Give the player a new gun or other functionality
                        break;
                    }
            }
            AudioManager.PlayClip.Invoke(5);
            StateTransition(new ItemControllerState_Used(this));
        }
    }

    private void getEntityInstanceData()
    {
        itemEntityDataInstance = Instantiate(itemEntityDataSource);
        itemEntityDataSource = null;
    }

    private void errorChecks()
    {
        bool errorFound = false;

        if (GetComponent<ActorController>())
        {
            Debug.LogError(name + ": ItemController has ActorController.cs attached to gameobject. These are mutually exclusive.");
            errorFound = true;
        }
        if (errorFound)
        {
            Debug.LogError(name + ": errorChecks() found at least one error that will cause an exception or break stuff. Quitting.");
            UnityEditor.EditorApplication.isPlaying = false;
        }
    }
    
    private void fields()
    {
        if (itemEntityDataInstance.MouseOverable)
        {
            if (!GetComponent<MouseOverComponent>())
                gameObject.AddComponent<MouseOverComponent>();
        }

        if (itemEntityDataInstance.OccupiesGrid)
        {
            if (!GetComponent<OccupiesGridComponent>())
                gameObject.AddComponent<OccupiesGridComponent>();
        }
        if (itemEntityDataInstance.Visible)
        {
            VisibleComponent visibleComponent;
            if (!GetComponent<VisibleComponent>())
                visibleComponent = gameObject.AddComponent<VisibleComponent>();
            else
                visibleComponent = GetComponent<VisibleComponent>();
            visibleComponent.BlocksLineOfSight = itemEntityDataInstance.BlocksLineOfSight;
        }
        if (itemEntityDataInstance.Interactable)
        {
            InteractComponent interactComponent;

            if (!GetComponent<InteractComponent>())
                interactComponent = gameObject.AddComponent<InteractComponent>();
            else
                interactComponent = GetComponent<InteractComponent>();

            interactComponent.Interaction.AddListener(pickUpItem);
        }
    }
}
