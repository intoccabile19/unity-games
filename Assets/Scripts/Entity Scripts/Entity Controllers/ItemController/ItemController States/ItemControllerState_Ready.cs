﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemControllerState_Ready : State
{
    public ItemControllerState_Ready(StateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void StateUpdate()
    {

    }

    public override void OnStateEnter()
    {
        debuglog(stateMachine.name + " entering ItemControllerState_Ready");
    }

    public override void OnStateExit()
    {
        debuglog(stateMachine.name + " exiting ItemControllerState_Ready");
    }
}
