﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemControllerState_Used : State
{
    public ItemControllerState_Used(StateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void StateUpdate()
    {
        debuglog(stateMachine.name + " destroying gameobject.");
        Object.Destroy(stateMachine.gameObject);
    }

    public override void OnStateEnter()
    {
        debuglog(stateMachine.name + " entering ItemControllerState_Used");
    }

    public override void OnStateExit()
    {
        debuglog(stateMachine.name + " exiting ItemControllerState_Used");
    }
}
