﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
#pragma warning disable 649 // disable warning for private field assigned but not used.
    
    public float MoveSpeed;
    public float Lifetime;
    public bool Hit;
    public int DamageAmount = 1;
    public AttackableComponent Target;
    float timer;
    void Start()
    {
    }

    void Update()
    {
        transform.Translate(transform.right * MoveSpeed * Time.smoothDeltaTime, Space.World);
        timer = timer + Time.deltaTime;
        if (timer > Lifetime)
        {
            if (Hit)
            {
                if (Target != null)
                    damageTarget();
            }
            Destroy(gameObject);
        }    
    }

    void damageTarget()
    {
        //Temp code, need to create 'Damageable.cs' to deal damage to
        Target.Damage(DamageAmount);
        debuglog("Dealing damage to actor");
    }

    //Move this code to an external toolbox class
    public void RotateIn2DToFace(GameObject subject, GameObject target)
    {
        Vector3 subjectPosition = new Vector3(subject.transform.position.x, subject.transform.position.y, subject.transform.position.z);
        Vector3 targetPosition = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z);

        Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 90) * (targetPosition - subjectPosition);

        subject.transform.rotation = Quaternion.LookRotation(Vector3.forward, rotatedVectorToTarget);
    }

    protected void debuglog(string message)
    {
        LogManager.Debug(this.GetType(), gameObject, message);
    }
}
