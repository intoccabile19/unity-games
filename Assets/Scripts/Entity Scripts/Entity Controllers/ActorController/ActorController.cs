﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections.Generic;

public class ActorController : MonoBehaviour
{
#pragma warning disable 649 // disable warning for private field assigned but not used.

    //Temp field: till we can do better with death animation
    private float iamdeadtimer = 3f;
    private float fadeRate = 0.6f;
    //Temp field: Move this to be a global constant
    private float sightRange = 15f;
    //Temp field: Move this to be a global constant
    private float moveSpeed = 5f;
    //Temp field: Move this to be a global constant or be based on weapon selected
    private float meleeRange = 1.5f;
    ////Temp field: Move this to be a global constant or be based on weapon selected
    private float activateRange = 1.5f;

    //Private fields to make accessible in the editor
    [SerializeField]
    private ActorEntityData entityDataSource;

    //Private fields not accessible in the editor
    private SceneManager sceneManager;
    private AIManager aiController;
    private SpriteRenderer spriteRenderer;
    private ProjectileController projectile;
    private GameObject slice;
    private bool aiControlled = true;
    private Transform movePoint;
    private Animator anim;
    private ActorEntityData actorEntityDataInstance;

    public enum fsmActorState
    {
        MayAct, Moving, Meleeing, Shooting, Waiting, Dying, Dead
    }

    public fsmActorState ActorState { get; private set; } = fsmActorState.Waiting;

    public bool CanShoot
    {
        get
        {
            return actorEntityDataInstance.CanShoot;
        }
    }

    public bool CanMelee
    {
        get
        {
            return actorEntityDataInstance.CanMelee;
        }
    }

    void Start()
    {
        getEntityInstanceData();

        errorChecks();
        
        fields();
        
        createGameObjects();

        EventManager.TurnManagerInstantiated.AddListener(registerTurn);
    }

    void Update()
    {
        //No need to write two separate state machines based on combat/exploration. Just pick out the specific methods that need to be different and tailor them (mainly movement)
        switch (sceneManager.SceneState)
        {
            case SceneManager.FSMSceneState.Exploration:
                {
                    switch (ActorState)
                    {
                        case fsmActorState.MayAct:
                            {
                                checkMyHealth();
                                break;
                            }
                        case fsmActorState.Moving:
                            {
                                //Need to add movement code here for moving multiple tiles in exploration mode
                                //Need to add code to allow actors to move freely in exploration mode
                                //AI movement/behaviour in exploration mode should be specified in the ai controller not here
                                if (Vector3.Distance(transform.position, movePoint.position) > .005f)
                                {
                                    moveActor();
                                }
                                else
                                {
                                    if (AStarManager.MyInstance.GetMyPath(this).Count > 1)
                                    {
                                        //Need to built .Pop() into the way this works from the ground up, i've used it in a hacky way here
                                        AStarManager.MyInstance.GetMyPath(this).Pop();
                                        moveInExploration(AStarManager.MyInstance.GetMyPath(this).Peek());
                                    }
                                    else
                                    {
                                        ActorState = fsmActorState.Waiting;
                                        anim.SetBool("moving", false);
                                        debuglog(name + ": Move complete. Switching to state Waiting and ending turn.");
                                    }
                                }
                                break;
                            }
                        case fsmActorState.Meleeing:
                            {
                                Debug.LogError(name + ": is in meleeing state even though scene state is exploration.");
                                break;
                            }
                        case fsmActorState.Shooting:
                            {
                                Debug.LogError(name + ": is in shooting state even though scene state is exploration.");
                                break;
                            }
                        case fsmActorState.Waiting:
                            {
                                checkMyHealth();
                                //TODO: Need a more graceful way to have player exit current state when scenestate changes
                                ActorState = fsmActorState.MayAct;
                                break;
                            }
                        case fsmActorState.Dying:
                            {
                                //would stick a dying animation here, and when the animation is complete, change to 'Dead' state
                                //for now just fade out then skip to dead

                                GetComponent<MouseOverComponent>().RemoveHighlight();
                                GetComponent<MouseOverComponent>().enabled = false;
                                GetComponent<AttackableComponent>().enabled = false;

                                if (spriteRenderer.color.a > 0.1)
                                {
                                    float alpha = spriteRenderer.color.a - (Time.deltaTime * fadeRate);
                                    Color newcolor = spriteRenderer.color;
                                    newcolor.a = alpha;
                                    spriteRenderer.color = newcolor;
                                }
                                else
                                    ActorState = fsmActorState.Dead;

                                break;
                            }
                        case fsmActorState.Dead:
                            {
                                //would stick a dead body sprite here and cease animating. Also spawn loot as one-off action on entry to this state.

                                GetComponent<AttackableComponent>().enabled = false;

                                //If player, hide sprite, count to 3 then end the running game in the editor
                                if (!aiControlled)
                                {
                                    spriteRenderer.enabled = false;
                                    iamdeadtimer = iamdeadtimer - Time.deltaTime;
                                    if (iamdeadtimer < 0f)
                                    {
                                        UnityEditor.EditorApplication.isPlaying = false;
                                        Debug.LogWarning("Player died. Quitting");
                                    }
                                }
                                
                                //For enemies, just destroy the gameobject
                                if (aiControlled)
                                {
                                    Destroy(movePoint.gameObject);
                                    aiController.UnregisterActor(this);
                                    Destroy(gameObject);
                                }
                                break;
                            }
                    }
                    break;
                }
            case SceneManager.FSMSceneState.Combat:
                {
                    switch (ActorState)
                    {
                        case fsmActorState.MayAct:
                            {
                                checkMyHealth();
                                break;
                            }
                        case fsmActorState.Moving:
                            {
                                if (Vector3.Distance(transform.position, movePoint.position) > .005f)
                                {
                                    moveActor();
                                }
                                else
                                {
                                    ActorState = fsmActorState.Waiting;
                                    anim.SetBool("moving", false);
                                    debuglog(name + ": Move complete. Switching to state Waiting and ending turn.");
                                    sceneManager.FinishMyTurn(gameObject);
                                }
                                break;
                            }
                        case fsmActorState.Meleeing:
                            {
                                Destroy(slice, .1f);
                                debuglog(name + ": Melee complete. Switching to state Waiting and ending turn.");
                                ActorState = fsmActorState.Waiting;
                                sceneManager.FinishMyTurn(gameObject);
                                break;
                            }
                        case fsmActorState.Shooting:
                            {
                                if (projectile == null)
                                {
                                    debuglog(name + ": Shooting complete. Switching to state Waiting and ending turn.");
                                    ActorState = fsmActorState.Waiting;
                                    sceneManager.FinishMyTurn(gameObject);
                                }
                                break;
                            }
                        case fsmActorState.Waiting:
                            {
                                checkMyHealth(); 
                                if (sceneManager.IsItMyTurn(gameObject))
                                {
                                    ActorState = fsmActorState.MayAct;
                                    debuglog(name + ": It is my turn. Switching to state May Act.");
                                }
                                break;
                            }
                        case fsmActorState.Dying:
                            {
                                //would stick a dying animation here, and when the animation is complete, change to 'Dead' state
                                //for now just fade out then skip to dead

                                GetComponent<MouseOverComponent>().RemoveHighlight(); 
                                GetComponent<MouseOverComponent>().enabled = false;
                                GetComponent<AttackableComponent>().enabled = false;

                                if (spriteRenderer.color.a > 0.1)
                                {
                                    float alpha = spriteRenderer.color.a - (Time.deltaTime * fadeRate);
                                    Color newcolor = spriteRenderer.color;
                                    newcolor.a = alpha;
                                    spriteRenderer.color = newcolor;
                                }
                                else
                                    ActorState = fsmActorState.Dead;

                                break;
                            }
                        case fsmActorState.Dead:
                            {
                                //would stick a dead body sprite here and cease animating. Also spawn loot as one-off action on entry to this state.

                                GetComponent<AttackableComponent>().enabled = false;

                                //If player, hide sprite, count to 3 then end the running game in the editor
                                if (!aiControlled)
                                {
                                    spriteRenderer.enabled = false;
                                    iamdeadtimer = iamdeadtimer - Time.deltaTime;
                                    if (iamdeadtimer < 0f)
                                    {
                                        UnityEditor.EditorApplication.isPlaying = false;
                                        Debug.LogWarning("Player died. Quitting");
                                    }
                                }

                                //For enemies, just destroy the gameobject
                                if (aiControlled)
                                {
                                    Destroy(movePoint.gameObject);
                                    aiController.UnregisterActor(this);
                                    Destroy(gameObject);
                                }
                                break;
                            }
                    }
                    break;
                }
        }
    }

    //This is called in select states within Update(). For example, it won't check your health and start killing you while in Moving state, to avoid you dying when halfway between tiles.
    private void checkMyHealth()
    {
        if (actorEntityDataInstance.CurrentHealth <= 0)
        {
            ActorState = fsmActorState.Dying;
            AudioManager.PlayClip.Invoke(4);
            //tell the scenemanager to remove my turn from the turn order
            sceneManager.RemoveFromTurnOrder(gameObject);
        }
    }

    //Single raycast from centre creates weird LOS check results. should probably check LOS from outer edges or at least further toward edges of current actor's cell
    public bool CheckLineofSight(VisibleComponent target)
    {
        int storeLayer = gameObject.layer;
        gameObject.layer = 2;

        ContactFilter2D filter = new ContactFilter2D();
        filter.useLayerMask = true;
        filter.layerMask = (1 << LayerMask.NameToLayer("BlocksLineofSight") | 1 << LayerMask.NameToLayer("Entities"));
        RaycastHit2D[] hits = new RaycastHit2D[500];
        Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), new Vector2(target.transform.position.x - transform.position.x, target.transform.position.y - transform.position.y), filter, hits, sightRange);
        List<RaycastHit2D> hitlist = hits.OfType<RaycastHit2D>().ToList();
        hitlist.RemoveAll(h => !h);

        gameObject.layer = storeLayer;

        foreach (RaycastHit2D h in hitlist)
        {
            if (h.collider.gameObject.layer == LayerMask.NameToLayer("BlocksLineofSight"))
            {
                //Raycast has hit a wall before hitting your target
                return false;
            }
            else if (h.collider.GetComponent<VisibleComponent>() != target && h.collider.GetComponent<VisibleComponent>().BlocksLineOfSight)
            {
                //Raycast has hit a an entity that has VisibleComponent set to block line of sight. Perhaps a door, or something like that, before hitting your target
                return false;
            }
            else if (h.collider.GetComponent<VisibleComponent>() == target)
            {
                //Raycast has hit your target before hitting anything else that would block it
                return true;
            }
        }

        return false;
    }

    //merge this with 'IsInProjectileRange' for general range check based on range of weapon selected
    public bool IsInMeleeRange(AttackableComponent target)
    {
        if (Vector3.Distance(transform.position, target.transform.position) <= meleeRange)
            return true;
        else
            return false;
    }


    //merge this with 'IsInMeleeRange' for general range check based on range of weapon selected
    public bool IsInProjectileRange(AttackableComponent target)
    {
        if (Vector3.Distance(transform.position, target.transform.position) <= actorEntityDataInstance.ProjectileRange)
            return true;
        else
            return false;
    }

    public bool IsInActivateRange(InteractComponent target)
    {
        if (Vector3.Distance(transform.position, target.transform.position) <= activateRange)
            return true;
        else
            return false;
    }

    public void ActionActivate(InteractComponent target)
    {
        if (ActorState == fsmActorState.MayAct)
        {
            target.Interact(this);
            ActorState = fsmActorState.Waiting;
            sceneManager.FinishMyTurn(gameObject);
        }
    }
    
    public void ActionMove(Vector3 moveDestination)
    {
        if (ActorState == fsmActorState.MayAct)
        {
            //This only moves a single square on the path currently
            movePoint.position = new Vector3(moveDestination.x + .5f, moveDestination.y + .8f, moveDestination.z);
            ActorState = fsmActorState.Moving;
            AudioManager.PlayClip.Invoke(1);
        }
        else
            Debug.LogError(name + ": Received direction to move but Actor is not in MayAct state.");
    }

    //Moving in exploration is a mess. Gotta be a much better way of doing it
    private void moveInExploration(Vector3 moveDestination)
    {
        movePoint.position = new Vector3(moveDestination.x + .5f, moveDestination.y + .8f, moveDestination.z);
        ActorState = fsmActorState.Moving;
        AudioManager.PlayClip.Invoke(1);
    }

    public void ActionMelee(AttackableComponent target)
    {
        //Consider moving shooting logic into a Melee Attack/Weapon component
        
        if (!target.enabled)
        {
            Debug.LogWarning(name + " was directed to attack an AttackableComponent that is disabled. Ignoring.)");
            return;
        }
        //add check here to ensure the requestor is the AIController / Player for this actor, error if not
        //add check to ensure distance is correct and line of sight is ok, error if not
        if (sceneManager.SceneState == SceneManager.FSMSceneState.Combat)
        {
            if (ActorState == fsmActorState.MayAct)
            {
                if (actorEntityDataInstance.CanMelee)
                {
                    instantiateMelee(target);
                    AudioManager.PlayClip.Invoke(3);
                    ActorState = fsmActorState.Meleeing;
                }
                else
                    Debug.LogError(name + ": Received direction to melee but the actor has canMelee set to false.");
            }
            else
                Debug.LogError(name + ": Received direction to melee but Actor is not in MayAct state.");
        }
        else
            Debug.LogError(name + ": Received direction to melee but SceneManager is not in Combat state.");
    }

    public void ActionShoot(AttackableComponent target)
    {
        //Consider moving shooting logic into a Ranged Attack/Weapon component
        if (!target.enabled)
        {
            Debug.LogWarning(name + " was directed to attack an AttackableComponent that is disabled. Ignoring.)");
            return;
        }
        
        //add check here to ensure the requestor is the AIController / Player for this actor, error if not

        if (sceneManager.SceneState == SceneManager.FSMSceneState.Combat)
        {
            if (ActorState == fsmActorState.MayAct)
            {
                if (actorEntityDataInstance.CanShoot && CheckLineofSight(target.GetComponent<VisibleComponent>()))
                {
                    //Todo: do you have a target to shoot? if no, then don't
                    //Todo: do you have line of sight to the target? if not, then don't shoot

                    instantiateProjectile(transform.position, target, rollToHitRanged());
                    AudioManager.PlayClip.Invoke(2);
                    ActorState = fsmActorState.Shooting;
                }
                else
                {
                    ActorState = fsmActorState.Waiting;
                    sceneManager.FinishMyTurn(gameObject);
                    debuglog(name + ": Received direction to shoot but the actor has canShoot set to false.");
                }
            }
            else
                Debug.LogError(name + ": Received direction to shoot but Actor is not in MayAct state.");
        }
        else
            Debug.LogError(name + ": Received direction to skip turn but SceneManager is not in Combat state.");
    }

    public void ActionSkipTurn()
    {
        //add check here to ensure the requestor is the AIController / Player for this actor, error if not
        if (sceneManager.SceneState == SceneManager.FSMSceneState.Combat)
        {
            if (ActorState == fsmActorState.MayAct)
            {
                sceneManager.FinishMyTurn(gameObject);
                ActorState = fsmActorState.Waiting;
                debuglog(name + ": skipped turn.");
                debuglog(name + ": Switching to state Waiting.");
            }
            else
                Debug.LogError(name + ": Received direction to skip turn but Actor is not in MayAct state.");
        }
        else
            Debug.LogError(name + ": Received direction to skip turn but SceneManager is not in Combat state.");
    }

    private bool rollToHitRanged()
    {
        //Use aim
        //You would also pass in the weapon being used etc
        //You would also pass in target defense
        //You would also look at any status effects
        int chancetohit = actorEntityDataInstance.Aim;
        int roll = Random.Range(1, 100);
        if (roll > chancetohit)
        {
            debuglog(name + " needed to roll " + chancetohit + " or less to hit and rolled " + roll + ". Missed");
            return false;
        }
        debuglog(name + " needed to roll " + chancetohit + " or less to hit and rolled " + roll + ". Hit");
        return true;
    }

    private bool rollToHitMelee()
    {
        //should not have a separate method for melee, this is just temp to give melee a higher hit chance
        int chancetohit = actorEntityDataInstance.Aim + 20;
        int roll = Random.Range(1, 100);
        if (roll > chancetohit)
        {
            debuglog(name + " needed to roll " + chancetohit + " or less to hit and rolled " + roll + ". Missed");
            return false;
        }
        debuglog(name + " needed to roll " + chancetohit + " or less to hit and rolled " + roll + ". Hit");
        return true;
    }

    void moveActor()
    {
        transform.position = Vector3.MoveTowards(transform.position, movePoint.position, moveSpeed * Time.deltaTime);
        anim.SetBool("moving", true);
    }

    void instantiateProjectile(Vector3 projectilePosition, AttackableComponent target, bool hit)
    {
        projectile = Instantiate(actorEntityDataInstance.Prefab_Projectile, projectilePosition, Quaternion.identity);
        projectile.Target = target;
        projectile.Hit = hit;
        if (hit)
        {
            projectile.RotateIn2DToFace(projectile.gameObject, target.gameObject);
            projectile.Lifetime = Vector3.Distance(projectile.transform.position, target.transform.position) / projectile.MoveSpeed;
        }
        else 
        {
            //Need to update this to aim just aside of the actor. Still aims directly at it
            projectile.RotateIn2DToFace(projectile.gameObject, target.gameObject);
            //And short/long a random distance, rather than the *1.5 below
            projectile.Lifetime = (Vector3.Distance(projectile.transform.position, target.transform.position) / projectile.MoveSpeed) * 1.5f;
        }
    }

    void instantiateMelee(AttackableComponent target)
    {
        if (slice == null)
        {
            slice = Instantiate(actorEntityDataInstance.Prefab_Melee, target.transform.position, Quaternion.identity);
            
            //TEMP CODE - I think we need a melee attack script where we can specify different properties of different attacks (timing, damage types, reach etc), just like projectiles.
            //This temp code should move to that script.
            //This rotation code is not quite the same as the projectile once since the 'subject' position used is the attacker rather than the attack itself. It's also off by 90deg by default because
            //the sprite itself is offset 90deg from the projectile sprite
            Vector3 subjectPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            Vector3 targetPosition = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z);
            Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, 0) * (targetPosition - subjectPosition);
            slice.transform.rotation = Quaternion.LookRotation(Vector3.forward, rotatedVectorToTarget);
            if (rollToHitMelee())
            {
                target.Damage(2);
                debuglog(name + ": Dealing 2 melee damage to " + target.name);
            }
        }
    }

    private void takeDamage(int amount)
    {
        actorEntityDataInstance.CurrentHealth = Mathf.Clamp(actorEntityDataInstance.CurrentHealth - amount, 0, actorEntityDataInstance.MaxHealth);
    }

    private void takeHealing(int amount)
    {
        actorEntityDataInstance.CurrentHealth = Mathf.Clamp(actorEntityDataInstance.CurrentHealth + amount, 0, actorEntityDataInstance.MaxHealth);
    }

    private void takeRepair(int amount)
    {
        actorEntityDataInstance.CurrentHealth = Mathf.Clamp(actorEntityDataInstance.CurrentHealth + amount, 0, actorEntityDataInstance.MaxHealth);
    }


    private void getEntityInstanceData()
    {
        actorEntityDataInstance = Instantiate(entityDataSource);
        entityDataSource = null;
    }

    //This code will quit the game and tell you what you fucked up. I added this because i got sick of broken references during testing.
    private void errorChecks()
    {
        //All controllers are mutually exclusive

        bool errorFound = false;

        if (FindObjectsOfType<AIManager>().Length != 1)
        {
            Debug.LogError(name + ": Found too few or too many aiControllers in the scene.");
            errorFound = true;
        }
        if (FindObjectsOfType<SceneManager>().Length != 1)
        {
            Debug.LogError(name + ": Found too few or too many scenemanagers in the scene");
            errorFound = true;
        }
        if (GetComponentsInChildren<SpriteRenderer>().Length != 1)
        {
            Debug.LogError(name + ": Found too few or too many spriterenderers on my child objects");
            errorFound = true;
        }
        if (!GetComponent<Animator>())
        {
            Debug.LogError(name + ": No animator component attached to gameobject.");
            errorFound = true;
        }
        if (!GetComponent<Collider2D>())
        {
            Debug.LogError(name + ": No collider component attached to gameobject.");
            errorFound = true;
        }
        if (GetComponent<ItemController>())
        {
            Debug.LogError(name + ": Actor has ItemController.cs attached to gameobject. These are mutually exclusive.");
            errorFound = true;
        }
        if (actorEntityDataInstance.CanMelee && actorEntityDataInstance.Prefab_Melee == null)
        {
            Debug.LogError(name + ": Can melee but has no melee prefab.");
            errorFound = true;
        }
        if (actorEntityDataInstance.CanShoot && actorEntityDataInstance.Prefab_Projectile == null)
        {
            Debug.LogError(name + ": Can shoot but has no projectile prefab.");
            errorFound = true;
        }
        if (errorFound)
        {
            Debug.LogError(name + ": errorChecks() found at least one error that will cause an exception or break stuff. Quitting.");
            UnityEditor.EditorApplication.isPlaying = false;
        }
    }

    //This code means we don't have to fuck around with dragging references into public fields in the editor, which only really works if you're not using prefabs anyway
    void fields()
    {
        if (GetComponent<PlayerController>())
            aiControlled = false;

        if (aiControlled)
        {
            aiController = FindObjectOfType<AIManager>();
            aiController.RegisterActor(this);
        }

        sceneManager = FindObjectOfType<SceneManager>();
        sceneManager.AddToTurnOrder(gameObject);
        
        spriteRenderer = GetComponentsInChildren<SpriteRenderer>()[0];

        anim = GetComponent<Animator>();

        if (actorEntityDataInstance.MouseOverable)
        {
            if (!GetComponent<MouseOverComponent>())
                gameObject.AddComponent<MouseOverComponent>();
        }

        if (actorEntityDataInstance.Attackable)
        {
            AttackableComponent attackableComponent;
            if (!GetComponent<AttackableComponent>())
                attackableComponent = gameObject.AddComponent<AttackableComponent>();
            else
                attackableComponent = GetComponent<AttackableComponent>();
            attackableComponent.Event_Damaged.AddListener(takeDamage);
        }

        if (actorEntityDataInstance.Healable)
        {
            HealableComponent healableComponent;
            if (!GetComponent<HealableComponent>())
                healableComponent = gameObject.AddComponent<HealableComponent>();
            else
                healableComponent = GetComponent<HealableComponent>();
            healableComponent.Event_Healed.AddListener(takeHealing);
        }

        if (actorEntityDataInstance.Repairable)
        {
            RepairableComponent repairableComponent;
            if (!GetComponent<RepairableComponent>())
                repairableComponent = gameObject.AddComponent<RepairableComponent>();
            else
                repairableComponent = GetComponent<RepairableComponent>();
            repairableComponent.Event_Repaired.AddListener(takeRepair);
        }

        if (actorEntityDataInstance.OccupiesGrid)
        {
            if (!GetComponent<OccupiesGridComponent>())
                gameObject.AddComponent<OccupiesGridComponent>();
        }

        if (actorEntityDataInstance.Visible)
        {
            VisibleComponent visibleComponent;
            if (!GetComponent<VisibleComponent>())
                visibleComponent = gameObject.AddComponent<VisibleComponent>();
            else
                visibleComponent = GetComponent<VisibleComponent>();
            visibleComponent.BlocksLineOfSight = actorEntityDataInstance.BlocksLineOfSight;
        }
    }

    private void createGameObjects()
    {
        GameObject empty = new GameObject();
        empty.name = name + " Move Point";
        movePoint = empty.transform;
        movePoint.position = transform.position;
    }

    protected void debuglog(string message)
    {
        LogManager.Debug(this.GetType(), gameObject, message);
    }

    private void registerTurn(TurnManager turnManager)
    {
        turnManager.AddToTurnOrder(this);
    }
}

